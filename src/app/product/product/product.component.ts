import { Component, OnInit } from '@angular/core';
import { IProduct } from '../../interfaces/product.interface';
import { ProductService } from '../product.service';
import { ShoppingCartService } from 'src/app/shopping-cart/shopping-cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  productsArray: IProduct[];
  selectedProduct: IProduct;
  quantity = 1;
  constructor(private productService: ProductService, private shoppingCartService: ShoppingCartService) { }


  ngOnInit() {
    this.productsArray = this.productService.getAll();
    this.selectedProduct = this.productsArray[0];
    console.log(this.productsArray)
  }

  addProduct() {
    this.shoppingCartService.addProduct(this.selectedProduct);
  }

  onProductSelected(productId) {
    // Resets previous selection
    this.selectedProduct = null;
    this.quantity = 1;

    // Finds the selected item
    this.selectedProduct = this.productsArray.find(e => e.id == productId);
    console.log(this.selectedProduct);
  }

  onAddItem(){
    this.selectedProduct.quantity = this.quantity;
    this.shoppingCartService.addProduct(this.selectedProduct);
  }

}
