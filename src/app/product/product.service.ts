import { Injectable } from '@angular/core';
import { IProduct } from '../interfaces/product.interface';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private products: IProduct[];

  constructor() {
    this.products = [
      { id: 1001, name: "Boots of Speed", price: 300 },
      { id: 1000, name: "Faerie Charm", price: 100 },
      { id: 1002, name: "Giant's Belt", price: 250 },
      { id: 1003, name: "Rejuvenation Bead", price: 50 }
    ]
  }

  getAll(){
    return this.products;
  }


}