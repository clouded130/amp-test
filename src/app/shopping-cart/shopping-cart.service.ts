import { Injectable } from '@angular/core';
import { ICart } from '../interfaces/cart.interface';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  private cart : ICart;
  productObservable = new Subject<ICart>();
  total : number
  constructor() { 
    this.cart = {
      total: 0,
      items: [],
    }
  }

  addProduct(product){
    this.cart.items.push(product);
    this.calculateTotal();
  }

  removeProduct(index){
    // Check if there is just one item or find item ro be removed
    if(this.cart.items.length === 1){
      this.cart.items = [];
    }
    else{
      this.cart.items.splice(index,1);
    } 
    this.calculateTotal();
    this.productObservable.next(this.cart)
  }

  
  calculateTotal(){
    this.cart.total = 0;
    this.cart.items.forEach(element => {
      this.cart.total += element.quantity * element.price;
    });
    // Emits changes int the cart
    this.productObservable.next(this.cart)
  }


}
