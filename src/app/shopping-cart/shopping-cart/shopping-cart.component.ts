import { Component, OnInit } from '@angular/core';
import { ICart } from 'src/app/interfaces/cart.interface';
import { ShoppingCartService } from '../shopping-cart.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {
  cart: ICart;

  constructor(private shoppingcartService: ShoppingCartService) { }

  ngOnInit() {
    this.shoppingcartService.productObservable.subscribe(
      (res: ICart) => {
        this.cart = res;
        console.log(res);
      }
    )
  }

  onRemoveItem(index){
    this.shoppingcartService.removeProduct(index);
  }

}
