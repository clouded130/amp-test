import { IProduct } from './product.interface';

export interface ICart{
    items: IProduct[];
    total: number;
}