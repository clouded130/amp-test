# AmpTest

# Task
Develop a simple Shopping cart application using latest version of angular.
- Please spend no more than two hours on this as will shown by commit timestamps, 
comment on what was done and what wasn't

## Completed items

- Add and Delete item from shopping cart
- List is in sync with changes on the cart

It is possible to add and remove items from the shopping cart and the list in the shopping cart is kept updated using an observable and there are two services to hold the shopping cart functions and to get the list of Products.

## TODO

- Edit item in the shopping cart
- Group items when they are the same 
- Save the cart in local storage
- Sorting/Filtering by category, name, price, etc
- Add Unit tests
- User experience improvements


## Angular 8.3.3

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
